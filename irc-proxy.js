var irc = require('irc');
var amqp = require('amqplib/callback_api');

var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_clients = {};
var servidoratual;
var hasping = false;

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
	
	conn.createChannel(function(err, ch) {
		
		amqp_conn = conn;
		amqp_ch = ch;
		
		inicializar();
	});
});

function inicializar() {
	
	receberDoCliente("registro_conexao", function (msg) {
		
		console.log('irc-proxy.js: recebeu registro de conexão');
		
		var id       = msg.id;
		var servidor = msg.servidor;
		var nick     = msg.nick;
		var canal    = msg.canal;
		
		irc_clients[id] = new irc.Client(
			servidor, 
			nick,
			{channels: [canal]}
		);		
			
		irc_clients[id].addListener('message'+canal, function (from, message) {
			
			console.log(from + ' => '+ canal +': ' + message);
			
			enviarParaCliente(id, {
				"timestamp": Date.now(), 
						   "nick": from,
				 "msg": message
			});
		});
		irc_clients[id].addListener('message', function (nick, to, text, message) {
			
			enviarParaCliente(id, {
				"timestamp": Date.now(), 
				"nick": nick,
				"msg": text
			});
		});
		irc_clients[id].addListener('error', function(message) {
			console.log('error: ', message);
		});
		irc_clients[id].addListener('whois', function(info) {
				
			servidoratual = info.server;

			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick": info.server,
				"msg": JSON.stringify(info)});
				
		});
		irc_clients[id].addListener('motd', function(motd) {
			 
			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick": motd.split(" ")[1],
				//Pra não sujar a tela do Cliente
				"msg": "Você Recebeu a Mensagem do Dia"}); 
			
		});
		irc_clients[id].addListener('nick', function(oldnick, newnick, channels, message) {

			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick": servidoratual,
				"msg": oldnick + " is now known as " + newnick});
			
		});
		irc_clients[id].addListener('names', function(channel, nicks) {
			var lista = channel+JSON.stringify(nicks);
		
			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick":servidoratual,
				"msg": lista});
		});
		irc_clients[id].addListener('raw', function(time) {
		
			if (time.command === "rpl_time"){

				enviarParaCliente(id, {
					"timestamp": Date.now(),
					"nick":servidoratual,
					"msg": time.args[2]});
			}
		});
		irc_clients[id].addListener('quit', function(nick, reason, channels, message) {

			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick":servidoratual,
				"msg": nick + " has left " + channels[0]});
			
		});
		irc_clients[id].addListener('join', function(channel, nick, message) {
				
			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick":servidoratual,
				"msg": nick + " has join to " + channel});
			
		});
		irc_clients[id].addListener('part', function(channel, nick, reason, message) {
			
		enviarParaCliente(id, {
			"timestamp": Date.now(),
			"nick":servidoratual,
			"msg": nick + " has left " + channel});
		
		});
		irc_clients[id].addListener('pong', function(pong) {

		   if(hasping){
			   
			enviarParaCliente(id, {
			   "timestamp": Date.now(),
			   "nick": pong,
			   "msg": "Você Recebeu um PONG"}); 
			}
			hasping = false;
	   });
		
		proxies[id] = irc_clients[id];
	});
	
	receberDoCliente("gravar_mensagem", function (msg) {
		   
		if(msg.msg.substring(0,5) == "/NICK" || msg.msg.substring(0,5) == "/nick"){
		   
		   var novonick = msg.msg.substring(6, msg.msg.length);
		   var semespaco = novonick.split(" ");
		   irc_clients[msg.id].send("NICK", semespaco[0]);
		   
		} else if(msg.msg == "/MOTD" || msg.msg == "/motd"){
		   
		   irc_clients[msg.id].send("MOTD");
		   
		} else if(msg.msg.substring(0,5) == "/JOIN" || msg.msg.substring(0,5) == "/join"){
		   
			var textomsg = msg.msg.split(" ");
			var novocanal = textomsg[1];
			if (novocanal.substring(0,1) == "#"){

				irc_clients[msg.id].join(novocanal);
			}else {

				irc_clients[msg.id].join("#"+novocanal);
			}
		} else if(msg.msg.substring(0,5) == "/PART" || msg.msg.substring(0,5) == "/part"){
			
			 var textomsg = msg.msg.split(" ");
			 var partcanal = textomsg[1];

			 if (partcanal.substring(0,1) == "#"){
				
				irc_clients[msg.id].part(partcanal);
			}else {
				
				irc_clients[msg.id].part("#"+partcanal);
			}
			
		 }else if(msg.msg == "/WHOIS" || msg.msg == "/whois"){
		   
		   irc_clients[msg.id].send("WHOIS", msg.nick);
		   
		} else if(msg.msg == "/NAMES" || msg.msg == "/names"){
		   
		   irc_clients[msg.id].send("NAMES", msg.canal);
		   
		   
		} else if (msg.msg == "/time" || msg.msg == "/TIME"){

			irc_clients[msg.id].send("TIME", servidoratual);

		}else if (msg.msg == "/ping" || msg.msg == "/PING"){
			
			irc_clients[msg.id].send("PING", servidoratual);
			hasping = true;
			
		}else if(msg.msg.substring(0,8) == "/PRIVMSG" || msg.msg.substring(0,8) == "/privmsg"){
			
			var textomsg = msg.msg.split(" ");
			var alvo = textomsg[1];
			var iniciomsg = alvo.length + 10;	
			var conteudomsg = msg.msg.substring(iniciomsg, msg.msg.length);
			irc_clients[msg.id].say(alvo, conteudomsg);
			 
			
		}else {
		irc_clients[msg.id].say(msg.canal, msg.msg);}
	});
}

function receberDoCliente (canal, callback) {
	
	amqp_ch.assertQueue(canal, {durable: false});
	
	console.log(" [irc] Waiting for messages in "+canal);
	
	amqp_ch.consume(canal, function(msg) {
		
		console.log(" [irc] Received %s", msg.content.toString());
		callback(JSON.parse(msg.content.toString()));
		
	}, {noAck: true});
}

function enviarParaCliente (id, msg) {
	
	msg = new Buffer(JSON.stringify(msg));
	
	amqp_ch.assertQueue("user_"+id, {durable: false});
	amqp_ch.sendToQueue("user_"+id, msg);
	console.log(" [irc] Sent to ID "+id+ ": "+msg);
}




