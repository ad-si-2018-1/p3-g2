# Manual do Desenvolvedor

### Objetivo 

O projeto 3 tem como objetivo desenvolver um sistema de troca de mensagens multiprotocolo, utilizando o protocolo AMQP para troca de mensagens entre produtores e consumidores, a interface Web para comunicação multi-chat, para envio e recebimento de mensgens AMQP, sendo que os canais de comunicação serão expresso através de URLs. Será criado um proxy AMQP/IR. 


### Tecnologias

- NodeJS
- JavaScript
- HTML
- Ajax
- RabbitMQ
- GIT
- HTTP


### Conceitos

#### NodeJS




#### Protocolo AMQP

O AMQP (Advanced Message Queuing Protocol) é um padrão aberto para passar mensagens de negócios entre aplicativos ou organizações. Ele conecta sistemas, alimenta processos de negócios com as informações de que eles precisam e transmite de forma confiável as instruções que atingem seus objetivos.

O AMQP permite que aplicativos enviem e recebam mensagens. A este respeito, é como mensagens instantâneas ou e-mail. Onde o AMQP difere enormemente é que ele permite especificar quais mensagens serão recebidas e de onde e como as compensações são feitas com relação à segurança, confiabilidade e desempenho. 

O middleware proprietário tem sido uma fonte de dependência, impedindo a concorrência no middleware por qualidade e custo e, muitas vezes, forçando a implementação de protocolos legados frágeis e inflexíveis. O AMQP permite que você forneça uma fonte dupla ou alterne fornecedores, estimule a concorrência e forneça uma plataforma para inovação

AMQP é um protocolo que define o comportamento que deve haver entre aplicativos e serviços sobre filas para que exista interoperabilidade, basicamente isto significa que o protocolo é wire-level, ao invés de definir funções e criar libs, ele apenas define uma sequencia de conversação pela rede, para que as coisas sejam feitas.

![Arquitetura AMQP](http://sao-paulo.pm.org/static/images/equinocio/2010/set/amqp-overview.png)



Vamos fazer uma analogia nessa abstração em três papeis, o publicador (publisher), o intermediário (broker) e os consumidores (consumer), como é ilustrado na imagem.

1. Fila de mensagens
É como o mailbox, no qual é um arquivo contendo os e-mails que são recebidos pelo usuário em um servidor de e-mail.

2. Consumidor
É como se um leitor de e-mail, que pode buscar ou apagar e-mails.

3. Exchanges
São como um MTA, no qual analisa o e-mail e decide, baseado na sua "tabela de roteamento" e então move a mensagem para uma ou mais mailbox.

4. Routing key
Corresponde ao To:, Cc: ou Bcc:, sem a informação de qual servidor é.

5. Um Exchange.
Cada instancia de um exchange, é como se fosse um processo de MTA separada.

6. Binding
Desempenha o mesmo papel de uma entrada na tabela de roteamento do MTA.


AMQP ele tem a habilidade de criar filas (mailboxes), exchanges (MTA) e bindings (routing) em tempo de execução. Existem outras diferenças também relacionadas como o servidor armazenar as mensagens, entre outros.


Exchange
É responsável por receber estas mensagens dos publicadores e rotear para as filas de mensagens baseado em critérios.

Message queue (Fila de mensagens)
Armazena as mensagens até que elas sejam consumidas com segurança pelos consumidores.

Bindings
Define a relação entre o Exchange e as filas de mensagens, o que define os critérios de roteamento.


#### RabbitMQ 

O RabbitMQ é um servidor de mensageria feito para suportar AMQP. Foi escrito em Erlang, roda nos principais sistemas operacionais, suporta enorme número de plataformas de desenvolvimento, é Open Source, sua licença é Mozzila 1.1 e pode ter suporte comercial.


### Softwares Necessários
- Editor de código fonte de sua preferência - VSCode, NetBeans, Notepad++, etc.
- NodeJS - https://nodejs.org/en/download/
- RabbitMQ - https://www.rabbitmq.com/#getstarted
- GIT - https://git-scm.com/downloads

###Executando o projeto

1. Clonar o projeto do endereço: https://gitlab.com/ad-si-2018-1/p3-g2.git
2. Quando o download tiver finalizado, navegue pelo terminal até a pasta do projeto e execute o comando npm install.
3. Este comando baixará as bibliotecas e dependências necessárias para a execução do projeto.  Necessário executar este comando somente uma vez após o download do projeto.
4. Agora você pode fazer novas implementações no projeto, ou alterar as funções já existentes conforme julgar necessário.
5. Quando achar necessário testar as implementações realizadas, navegue pelo terminal até a pasta src do projeto e execute os comando node app.js e node irc-proxy.js. O projeto estará rodando na porta 3000.
Acesse o link http://localhost:3000 através de qualquer navegador, e realize todos os testes que julgar necessário.

Observação: Depois do comando npm install instale todos os softwares necessários principalmente o rabbitmq, pois sem ele este codigo não é executado.


