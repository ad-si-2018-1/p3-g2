# Introdução

O projeto 3 tem como objetivo desenvolver um sistema de troca de mensagens multiprotocolo, utilizando o protocolo AMQP para troca de mensagens entre produtores e consumidores, a interface Web para comunicação multi-chat, para envio e recebimento de mensgens AMQP, sendo que os canais de comunicação serão expresso através de URLs. Será criado um proxy AMQP/IR. 


# Tecnologias utilizadas

1. Protocolo AMPQ

# Protocolo AMPQ

O AMQP significa Advanced Message Queuing Protocol (Protocolo avançado para enfileiramento de Mensagens)  é um protocolo de comunicação em rede, que permite que aplicações se comuniquem. Tem como seu principal propósito permitir a total interoperabilidade funcional entre clientes e servidores middleware de mensagens (conhecidos também como brokers).

AMQP define um protocolo padrão para publicação, enfileiramento, armazenamento e consumo de mensagens

## Vantagens





   