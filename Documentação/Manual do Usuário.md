# MANUAL DO USUÁRIO

## Requisitos para a execução do projeto

- NodeJS - https://nodejs.org/en/download/
- RabbitMQ - https://www.rabbitmq.com/#getstarted
- GIT - https://git-scm.com/downloads

# Executando o Projeto

1. Clonar o projeto do endereço: https://gitlab.com/ad-si-2018-1/p3-g2.git 
   use o comando no prompt de comando do windowns ou no terminal do linux
   git clone [link a ser clonado]
2. Após o termino do download execute o comando npm install, para baixar as bibliotecas e dependências necessárias para a execução do projeto. Necessário apenas um vez.
3. Execute os comando node app.js e node irc-proxy.js. O projeto estará rodando na porta 3000.
4. Provavelmente nesta etapa a execução corre o risco de dar errado, é porque no seu computador não foi instalado o rabbitmq, instale a biblioteca atraves do link acima 
   nos requisitos.
5. Acesse o link http://localhost:3000 através de qualquer navegador.
6. Preencher seu nick, o canal o qual quer entrar e o servidor (Ex: joaojose, #AD, irc.freenode.net) para efetuar o login no chat.

# COMANDOS:

  - Todos os comandos e mensagens são enviados ao apertar a tecla ENTER.

  - Para enviar uma mensagem ao canal escolhido no login basta digitar a mensagem. Ex: Ola pessoal + Enter. A mensagem será enviada ao canal escolhido no login.
  
  - /NICK NOVONICK - Altera o seu nick atual. Ex: /NICK joaojose1.
  - /MOTD - Solicita que o servidor IRC envie a mensagem do dia para você.
  - /JOIN CANAL - Faz com que você se conecte a um canal, você pode se conectar a mais de um canal. Ex: /JOIN #AD2.
  - /PART CANAL - Deixa um canal o qual foi conectado anteriormente. Ex: /PART #AD2.
  - /WHOIS - Vê as informações sobre você, que estão guardadas no servidor IRC.
  - /NAMES - Mostra a lista de usuários conectados ao canal que você escolheu ao fazer login.
  - /TIME  - Mostra a hora do servidor IRC.
  - /PING - Envia um Ping para o servidor IRC. O servidor responderá com um PONG.
  - /PRIVMSG NICK MENSAGEM - Envia uma mensagem privada para um usuário. Ex: /PRIVMSG joaojunior23 "Olá João Junior Tudo bom?"
